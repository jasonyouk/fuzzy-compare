# Fuzzy-compare #

This is a simple script that performs fuzzy string matching on 2 files and outputs the scores of various matching algorithms. Input files should be text based (txt or csv will do) and should have 1 word/term/sentence per line. Every term on file1 will be paired with every term on file2 and scores are calculated for each pair. You can provide a minimum score for the algorithm of your choice to filter out your results (but all scores are output anyway).

I did this because I had to do string comparisons on big files on a regular basis and was unsure about what the best fuzzy algorithm was. This way I can check scores side by side and decide depending on the use case.

Calculated scores:

* Jaro
* Jaro-Winkler
* Ratio (see [Python's difflib.SequenceMatcher](https://docs.python.org/2/library/difflib.html) for details)
* Partial Ratio (*)
* Token Sort Ratio (*)
* Token Set Ratio (*)

*for detailed explanations of these algorithms [check this page by the author of the fuzzywuzzy](http://chairnerd.seatgeek.com/fuzzywuzzy-fuzzy-string-matching-in-python/) library himself.

### Multiprocessing ###

Included in the repo are also 2 additional versions of this script which are an attempt at making it faster. They use Python's multiprocessing module and can be much faster, especially for large files:

* **fuzzycompare_multiproc_map.py** - uses the Pool.map function and is pre-configured with 4 processes. Just edit the code to change that. Disadvantage: results are only written to disk at the very end, which means that when processing large files they'll be kept in memory until then, so beware.
* **fuzzycompare_multiproc_queues.py** - this is my attempt to fix the memory usage problem by using queues. Here I have a job queue and a write queue. The write queue is handled by a single process to avoid I/O conflicts. It works, although for some reason it's even slower than the simple single-process version when processing the sample data provided (maybe because of the overhead in managing the additional processes, but most probably because I'm doing something wrong :))

For the sample data provided here these are the times I get on my machine (Macbook Pro Retina, 2.3 GHz Intel Core i7 with 16GB of RAM):

* **fuzzycompare.py**: 0:00:47
* **fuzzycompare_multiproc_map.py**: 0:00:13
* **fuzzycompare_multiproc_queues.py**: 0:01:04


### How do I get set up? ###

Just clone this repo or copy the script locally and run it. I have some sample data to test this out as well.

I'm using a bunch of 3rd party libraries so make sure you install them first:

* progressbar - https://code.google.com/p/python-progressbar/
* slugify - https://github.com/slugify/slugify
* fuzzywuzzy - https://github.com/seatgeek/fuzzywuzzy
* levenshtein - https://github.com/ztane/python-Levenshtein/

This was only tested on Mac OS X Yosemite with Python 2.7.

### Usage ###

```
#!bash

usage: fuzzycompare.py [-h] --minscore MINSCORE --algorithm
                       {jaro,jaro_winkler,simple_ratio,partial_ratio,token_sort_ratio,token_set_ratio}
                       file_to_match file_to_be_matched
```
The minimum score can be provided as an int between 0 and 100 or a float between 0 and 1.

### Example ###
```
#!bash

python fuzzycompare_multiproc_map.py -a token_sort_ratio -s 80 data/keuken.txt data/searches.txt
```